package learning.util.testing;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SystemOutRule {

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

	public void enableLog() {
		baos.reset();
		System.setOut(new PrintStream(baos));
	}

	public String getLog() {
		return baos.toString();
	}
}
