package learning.practice.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import learning.practice.Classroom;
import learning.practice.Student;
import learning.util.testing.SystemOutRule;

public class StreamTest {
	
	final List<Classroom >classrooms = new ArrayList<Classroom>();
	final SystemOutRule rule = new SystemOutRule();
	
	@BeforeEach
	public void init() {
		rule.enableLog();
		classrooms.add(Classroom.of("java8", 
				                    Stream.of("Monday","Tuesday","Wednesday").collect(Collectors.toList()), 
				                    Student.of(Stream.of("Fernando;23","Estrela;31","Carvalho;45","João;34")
				                    		.collect(Collectors.toList()))));
		classrooms.add(Classroom.of("java7", 
                Stream.of("Monday","Thursday","Saturday").collect(Collectors.toList()), 
                Student.of(Stream.of("Julia;23","Carlos;32","Antonio;40","João;34").collect(Collectors.toList()))));
		
		classrooms.add(Classroom.of("Go", 
                Stream.of("Friday","Sunday").collect(Collectors.toList()), 
                Student.of(Stream.of("Julia;23","Carlos;32","Antonio;40","João;34").collect(Collectors.toList()))));
	}
	
	@Test
	@DisplayName("Printa todas as aulas")
	public void showNames() {
		Classroom.collectNames(classrooms).stream().forEach(System.out::println);
		Assertions.assertEquals("java8\njava7\nGo\n", rule.getLog()); 
	}
	
	@Test
	@DisplayName("Busca todos os estudantes que o nome da aula contema palavra java, a aula é na segunda e o aluno tem 23 anos.")
	public void filter() {
		classrooms.stream()
				.filter(classroom -> classroom.getName().contains("java"))
				.filter(classroom -> classroom.getDays().contains("Monday"))
					.forEach(classroom -> classroom.getStudents()
							.stream().filter(studant ->studant.getAge().equals(23))
								.forEach(studant -> System.out.println(studant.getName()))
							);
		
		Assertions.assertEquals("Fernando\nJulia\n", rule.getLog()); 
	}
	
	@Test
	@DisplayName("Soma a idade de todos os alunos da primeira turma")
	public void sum() {
		int expected = 133;
		int actual = classrooms.stream().findFirst().orElse(null).getStudents().stream().mapToInt(s -> s.getAge()).sum(); 
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	@DisplayName("Retrona o aluno da aulo Go com menor idade")
	public void min() {
		int expected = 23;
		OptionalInt actual = classrooms.stream().
				filter(classroom -> classroom.getName().equals("Go"))
				.findAny().orElseThrow().getStudents().stream().mapToInt(s -> s.getAge()).min(); 
		Assertions.assertEquals(expected, actual.getAsInt());
	}
	
	@Test
	@DisplayName("Retrona o aluna da aulo java8 com maior idade")
	public void max() {
		int expected = 45;
		OptionalInt actual = classrooms.stream().
				filter(classroom -> classroom.getName().equals("java8"))
				.findAny().orElseThrow().getStudents().stream().mapToInt(s -> s.getAge()).max(); 
		Assertions.assertEquals(expected, actual.getAsInt());
	}
	
	@Test
	@DisplayName("Printa a aula e a media de idade dos alunos")
	public void average() {
		String expected = "java8: 33.25\n" + 
				"java7: 32.25\n" + 
				"Go: 32.25\n";
		
		classrooms.stream()
			.forEach(classroom -> System.out.println(
					classroom.getName()+": "
					+ classroom.getStudents().stream().mapToInt(
								s -> s.getAge()
							).average().getAsDouble())
					);
		 
		Assertions.assertEquals(expected, rule.getLog());
	}
	
	@Test
	@DisplayName("Busca uma aula que comece com java")
	public void noneMatch() {
		boolean actual = classrooms.stream()
				.noneMatch(cr -> cr.getName().startsWith("java"));
		Assertions.assertFalse(actual);
	}
	
	@Test
	@DisplayName("Printa todos os alunos em de todas as aluas")
	public void flatMap() {
		Stream<Student> result =  classrooms.stream().flatMap(cr -> cr.getStudents().stream());
		Student[] s =  result.distinct().toArray(Student[]::new);
		Assertions.assertEquals("[Student [name=Fernando, age=23], "
				+ "Student [name=Estrela, age=31], "
				+ "Student [name=Carvalho, age=45], "
				+ "Student [name=João, age=34], "
				+ "Student [name=Julia, age=23], "
				+ "Student [name=Carlos, age=32], "
				+ "Student [name=Antonio, age=40], "
				+ "Student [name=João, age=34], "
				+ "Student [name=Julia, age=23], "
				+ "Student [name=Carlos, age=32], "
				+ "Student [name=Antonio, age=40], "
				+ "Student [name=João, age=34]]", Arrays.deepToString(s));
	}
	
	@Test
	@DisplayName("Printa todos as aluas concatenadas")
	public void reduce() {
		String result = classrooms.stream().map(cr -> cr.getName()).reduce("", String::concat);
		Assertions.assertEquals("java8java7Go", result);
	}
	
	@Test
	@DisplayName("Printa todos as aluas concatenadas separadas por ;")
	public void reduce2() {		
		String result = classrooms.stream().map(Classroom::getName).collect(Collectors.joining(";"));
		Assertions.assertEquals("java8;java7;Go", result);
	}
	
	
	
}
