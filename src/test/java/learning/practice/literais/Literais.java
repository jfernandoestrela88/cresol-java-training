package learning.practice.literais;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Literais {
	
	@Test
	public void literaisOctal() {
		int var = 01;
		Assertions.assertEquals(1,var);
	}	
	
	@Test
	public void literaisExaDecimal() {
		int var = 0x1;
		Assertions.assertEquals(1,var);
	}

	@Test
	public void literaisBinario() {
		int var = 0b100;
		Assertions.assertEquals(4,var);
	}
	
	@Test
	public void literaisUndescor() {
		int var = 100_000_00;
		Assertions.assertEquals(10000000,var);
	}
}
