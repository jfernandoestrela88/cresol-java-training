package learning.practice.io;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FilesTest {
	
	@Test
	@DisplayName("pega primeira linha")
	public void lines() {
		try(Stream<String> lines =  Files.lines(Paths.get("./src/test/java/learning/practice/io/FilesTest.java"));){
			String actual = lines.findFirst().orElse("");
			String expected = "package learning.practice.io;";
			Assertions.assertEquals(expected, actual);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Test
	@DisplayName("pega arquivos e diretorios")
	public void walk() {
		try(Stream<Path> walk =  Files.walk(Paths.get("./src/test/java"), FileVisitOption.FOLLOW_LINKS);){
			List<String> list = walk.map(f -> f.getFileName().toString()).collect(Collectors.toList());
			Assertions.assertEquals("[java, learning, "
					+ "util, testing, "
					+ "SystemOutRule.java, "
					+ "domains, movies, MoviesTest.java, "
					+ "functions, strings, RepeaterTest.java, "
					+ "practice, stream, StreamTest.java, io, FilesTest.java, "
					+ "BufferedReaderTest.java]", list.toString());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
