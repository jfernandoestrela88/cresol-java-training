package learning.practice.io.newio7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NewIoJava7Test {
	
	private static final String THIS_PATH = "./src/test/java/learning/practice/io/newio7/NewIoJava7Test.java";
	private static final String FIRST_LINE = "package learning.practice.io.newio7;";
	private static final String NEW_PATH = "./src/test/java/learning/practice/io/newio7/test";
	
	@Test
	public void readFiles() {
		Assertions.assertTrue(Files.exists(Paths.get(THIS_PATH)));
	}
	
	@Test
	public void creatDiretory() throws IOException {
		Path path = Paths.get(NEW_PATH);
		Files.createDirectories(path);
		Assertions.assertTrue(Files.isDirectory(path));
		Files.delete(path);
		Assertions.assertFalse(Files.isDirectory(path));
	}
	
	@Test
	public void readAllLines() throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(THIS_PATH));
		Assertions.assertTrue(lines.get(0).equals(FIRST_LINE));
	}

}
