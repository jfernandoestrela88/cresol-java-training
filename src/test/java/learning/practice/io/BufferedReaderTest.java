package learning.practice.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BufferedReaderTest {
	
	@Test
	@DisplayName("pega primeira linha")
	public void bufferedReader() {
		try(BufferedReader render = new BufferedReader(new FileReader("./src/test/java/learning/practice/io/BufferedReaderTest.java"));
			Stream<String> lines = render.lines()	){
			Assertions.assertEquals("package learning.practice.io;", lines.findFirst().orElse("")); 
		}catch (IOException io) {
			throw new RuntimeException(io);
		}
	}
	
	@Test
	@DisplayName("pega a linha desse metodo")
	public void bufferedReader2() {
		try(BufferedReader render = new BufferedReader(new FileReader("./src/test/java/learning/practice/io/BufferedReaderTest.java"));
			Stream<String> lines = render.lines()	){
			String lineResult =  lines.filter(line -> line.contains("public void bufferedReader2()")).findAny().orElse("");
			Assertions.assertEquals("	public void bufferedReader2() {", lineResult); 
		}catch (IOException io) {
			throw new RuntimeException(io);
		}
	}

}
