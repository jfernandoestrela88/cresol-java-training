package learning.practice.forkjoin;

import java.util.concurrent.ForkJoinPool;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FibonacciTest {
	
	@Test
	public void test() {
		ForkJoinPool pool = new ForkJoinPool(4);
		Fibonacci fibonacci = new Fibonacci(10);
		long resultado = pool.invoke(fibonacci);
		Assertions.assertEquals(55, resultado);
	}

}
