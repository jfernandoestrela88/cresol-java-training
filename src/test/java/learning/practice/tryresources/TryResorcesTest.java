package learning.practice.tryresources;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TryResorcesTest {
	
	@Test
	public void tryWithResource() throws Exception {
		AutoCloseable resource = () -> System.out.println("Close resource");
		try(AutoCloseable resource2 = resource){
			System.out.println("doing");
		}
		try {	
			resource.close();
		}catch (Exception e) {
			Assertions.assertTrue(true);
		}
	}
	
	@Test
	public void tryWithResource2() throws Exception {
		try(AutoCloseable resource = () -> {throw new IllegalArgumentException("deu erro close");} ){
			throw new SQLException();
		}catch (SQLException e) {
			Assertions.assertTrue(e.getSuppressed()[0] instanceof IllegalArgumentException);
		}
	}
	
	@Test
	public void multCath() {
		try {
			DriverManager.getConnection("");
			new File("").createNewFile();	
		} catch (IOException | SQLException e) {
			Assertions.assertTrue(e instanceof SQLException);
		}
	}

}
