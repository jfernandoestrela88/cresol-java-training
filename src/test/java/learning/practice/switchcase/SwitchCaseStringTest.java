package learning.practice.switchcase;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import learning.util.testing.SystemOutRule;

public class SwitchCaseStringTest {
	
	final SystemOutRule rule = new SystemOutRule();
	
	@Test
	public void switchString() {
		rule.enableLog();
		test("João");
		test("Luiz");
		Assertions.assertEquals("O João é um Gerente\nO Luiz é um Pião\n", rule.getLog());
	}
	
	private void test(String name) {
		switch (name) {
		case "João":
			printName(name,"Gerente");
			break;
		case "Luiz":
			printName(name,"Pião");
			break;	

		default:
			throw new IllegalArgumentException("Unexpected value: " + name);
		}
	}

	private void printName(String name,String cargo) {
		System.out.println("O ".concat(name).concat(" é um ".concat(cargo)));
		
	}

}
