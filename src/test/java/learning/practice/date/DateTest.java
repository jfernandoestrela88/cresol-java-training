package learning.practice.date;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateTest {
	
	@Test
	public void of() {
		LocalDate expected = LocalDate.of(2019, Month.JANUARY,1);
		LocalDate actual = LocalDate.of(2018, Month.DECEMBER,31).plusDays(1);
		Assertions.assertEquals(expected.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				                actual.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		
	}
	
	@Test
	public void at() {
		LocalDateTime expected = LocalDateTime.now(ZoneId.of("Japan"));
		LocalDateTime actual = LocalDateTime.now(ZoneId.of("Brazil/East"));
		Assertions.assertTrue((actual.getHour() -expected.getHour())== 12);
	}
	
	@Test
	public void betweenDuration() {
		LocalDateTime timeIni = LocalDateTime.of(2019, 6, 23, 12, 54);
		LocalDateTime timeFim = LocalDateTime.of(2019, 6, 23, 12, 53);
		Duration duration = Duration.between(timeFim, timeIni);
		Assertions.assertTrue(duration.toMillis() == 60000) ;
	}
	
	@Test
	public void betweenPeriodo() {
		LocalDate timeIni = LocalDate.of(2018, 5, 4);
		LocalDate timeFim = LocalDate.of(2019, 6, 30);
		Period period = Period.between( timeIni,timeFim);
		Assertions.assertTrue(period.getYears() == 1) ;
		Assertions.assertTrue(period.getMonths() == 1) ;
		Assertions.assertTrue(period.getDays() == 26) ;
	}

}
