package learning.domains.movies;

import java.util.Arrays;
import java.util.Collection;
import java.util.IntSummaryStatistics;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import learning.util.testing.SystemOutRule;

public class MoviesTest {

	final Movies movies = Movies.of(Movie.of("Ghost",1981, 12, 1),Movie.of("Black Panther",2015, 1, 20));
	final SystemOutRule rule = new SystemOutRule();
	
	@BeforeEach
	public void out() {
		rule.enableLog();
	}
	
	@Test
	public void selectTitles() {
		String[] expected = new String[] {"Ghost","Black Panther"};
		Assertions.assertEquals(Arrays.asList(expected), movies.selectTitles());
	}
	
	@Test
	public void showTitles() {
		Collection<String> titles = movies.selectTitles();
		titles.stream().forEach(System.out::println);
		Assertions.assertEquals("Ghost\nBlack Panther\n", rule.getLog());
	}
	
	@Test
	@DisplayName("Define a pipeline using of, map, filter, forEach, peek functional operations")
	public void forEach() {
		//Movie[] array = movies.selectAll().toArray(Movie[]::new);
		Stream<Movie> stream = movies.selectAll().stream();
		//stream = Stream.of(array);
		stream
			.peek(System.out::println)
			.filter(m -> m.year > 1990)
			.peek(System.out::println)
			.map(m -> m.title)
			.forEach(t -> System.out.print(t));
		//TODO
		Assertions.assertEquals("Movie [title=Ghost, year=1981]\n" + 
				"Movie [title=Black Panther, year=2015]\n" + 
				"Movie [title=Black Panther, year=2015]\n" + 
				"Black Panther", rule.getLog());
	}
	
	@Test
	public void sum() {
		int expected = 3996;
		int actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .sum();
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void average() {
		double expected = 1998.0;
		double actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .average().orElse(0);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void summaryStats() {
		IntSummaryStatistics summaryStatistics = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .summaryStatistics();
		Assertions.assertEquals(2, summaryStatistics.getCount());
		Assertions.assertEquals(1998, summaryStatistics.getAverage());
		Assertions.assertEquals(1981, summaryStatistics.getMin());
		Assertions.assertEquals(2015, summaryStatistics.getMax());
		Assertions.assertEquals(3996, summaryStatistics.getSum());
	}
	
	@Test
	public void min() {
		double expected = 1981;
		double actual = movies.selectAll().stream()
			  .mapToInt(m -> m.year)
			  .min().orElseThrow(IllegalArgumentException::new);
		Assertions.assertEquals(expected, actual);
	}
	
	@Test
	public void optional1() {
		String title = null;
		Optional.ofNullable(title).ifPresent(t -> System.out.println(t));
		Assertions.assertEquals("",rule.getLog());
	}
	
	@Test
	public void optional2() {
		String title = "Jurassic World";
		Optional.ofNullable(title).ifPresent(t -> System.out.println(t));
		Assertions.assertEquals("Jurassic World\n",rule.getLog());
	}
	
	@Test
	public void findFirst() {
		Optional<Movie> first = movies.selectAll().stream().findFirst();
	}
	
	@Test
	public void findAny() {
		Optional<Movie> any = movies.selectAll().stream().findAny();
	}
	
	@Test
	public void noneMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.noneMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertFalse(actual);
	}
	
	@Test
	public void allMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.allMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertFalse(actual);
	}
	
	@Test
	public void anyMatch() {
		boolean actual = movies.selectAll()
				.stream()
				.anyMatch(m -> m.title.startsWith("Gh"));
		Assertions.assertTrue(actual);
	}
	
	@Test
	public void range() {
		IntStream.range(0, 10).sum();
		IntStream.rangeClosed(1, 10).count();
	}
	
	@Test
	public void xx() {
		Random random = new Random();
		IntStream stream = IntStream.generate(() -> random.nextInt());
		Assertions.assertEquals(0,stream.skip(3).limit(100).count());
	}
}
