package learning.domain.basketball;

import java.util.Arrays;
import java.util.Comparator;

public class Teams {

	private Team[] data;
	
	private Teams(Team... data){
		this.data = data;
	}
	
	static Teams of(Team... data) {
		return new Teams(data);
	}
	
	static Teams nba() {
		return of(
				new Team("Boston Celtics",7), 
				new Team("Los Angeles Lakers",23), 
				new Team("Detroit Pistons",16), 
				new Team("Golden State Warriors",2)
		);		
	}
	
	Team[] sortByName() {
		Arrays.sort(data,(t1,t2) -> t1.name.compareTo(t2.name));
		return data;
	}
	
	Team[] sortByPosition() {
		Arrays.sort(data,(t1,t2) -> t1.position - t2.position);
		return data;
	}
	
	Team[] sort(Comparator<Team> sortBy) {
		Arrays.sort(data,sortBy);
		return data;
	}
	
}
