package learning.domain.basketball;

import java.util.Arrays;
import java.util.List;

public class Team {

	public final String name;
	public final int position;

	//TODO field can be used not final

	Team(String name, int position){
		this.name = name;
		this.position = position;
	}
	
	@Override
	public String toString() {
		return "Team [name=" + name + ", position=" + position + "]";
	}

	@FunctionalInterface
	interface Printer {
		public abstract void print(Object object);
	}
	
	interface Builder {
		Team build(String name, int position);
	}

	interface X {
		String x();
	}

	public static void main(String[] args) {
		Builder builder = Team::new;	
		List<String> names = Arrays.asList("Vinny");
		String[] namesAsArray = names.toArray(new String[0]);
		namesAsArray = names.toArray(String[]::new);
		
		Team minnesota = builder.build("Minnesota", 11);
		
		Teams teams = Teams.nba();
		
		Printer sysout = o -> System.out.println(o);
		Printer sysout2 = o -> System.out.print("----"+o+"----");
		
		show("Sort by name",teams.sortByName(), o -> System.out.println(o));
		show("Sort by position",teams.sortByPosition(), sysout2);
		show("Sort by name desc",teams.sort((t1,t2) -> t2.name.compareTo(t1.name)), sysout);
		show("Sort by position desc",teams.sort(Team::sortByPositionDesc), sysout);
	}
	
	public static final int sortByPositionDesc(Team t1, Team t2) {
		return t2.position - t1.position;
	}
	public static final void show(String title, Team[] teams, Printer printer) {
		show(title, printer);
		for(Team team: teams) show(team, printer);
	}
	
	public static final void show(Object obj, Printer printer) {
		printer.print(obj);
	}
	
	
}
