package learning.domain.basketball;

import java.util.function.Function;

public class GameFunctions {

	public static void main(String[] args) {
		Game game1 = new Game("BOS","LAL",90,80);
		String score = GameFunctions.getScore2(game1);
		System.out.println(score);
	}
	
	public static String getScore(Game game) {
		Scorer scorer = g -> 
		g.away + " "+g.awayPoints + " x " + g.homePoints + " " + g.home;
		scorer = g -> 
		g.away + " "+g.awayPoints + "," + " " + g.home + " "+g.homePoints;

		return scorer.calculate(game);
	}
	
	public static String getScore2(Game game) {
		Function<Game,String> scorer = g -> 
		g.away + " "+g.awayPoints + " x " + g.homePoints + " " + g.home;
		scorer = g -> 
		g.away + " "+g.awayPoints + "," + " " + g.home + " "+g.homePoints;

		return scorer.apply(game);
	}
	
	interface Scorer {
		String calculate(Game game);
	}
}
