package learning.functions.strings;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Repeater {

	String repeat(String text, int count);

	public static String usingVersion11(String text, int count) {
		return text.repeat(count);
	}

	public static String usingVersion8(String text, int count) {
		return IntStream.rangeClosed(1, count).mapToObj(i -> text).collect(Collectors.joining());
	}

	public static String usingVersion5(String text, int limit) {
		StringBuilder sb = new StringBuilder();
		for (int counter = 1; counter <= limit; counter++) {
			sb.append(text);
		}
		return sb.toString();
	}

}
