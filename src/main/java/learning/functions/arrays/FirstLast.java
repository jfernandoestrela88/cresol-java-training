package learning.functions.arrays;

public interface FirstLast<T> {
	
	T[] getArray();

	default int firstPosition() {
		return 0;
	}
	default int lastPosition() {
		return getArray().length - 1;
	}
	default boolean notEmpty() {
		return !empty();
	}
	default boolean empty() {
		return getArray().length == 0;
	}
	default T first() {
		if (empty()) throw new IllegalStateException("Cannot get first. Reason: array is empty");
		return getArray()[firstPosition()];
	}

	default T last() {
		if (empty()) throw new IllegalStateException("Cannot get last. Reason: array is empty");
		return getArray()[lastPosition()];
	}
	
}
