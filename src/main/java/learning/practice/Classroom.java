package learning.practice;

import java.util.List;
import java.util.stream.Collectors;

public class Classroom {
	
	public static Classroom of(String name,List<String> days,List<Student> students) {
		return new Classroom(name, days, students);
	}
	
	private String name;
	private List<String> days;
	private List<Student> students;
	
	public static List<String> collectNames(List<Classroom> classrooms) {
		return classrooms.stream().map(classroom -> classroom.name).collect(Collectors.toList());
	}

	public Classroom(String name, List<String> days, List<Student> students) {
		super();
		this.name = name;
		this.days = days;
		this.students = students;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getDays() {
		return days;
	}
	public void setDays(List<String> days) {
		this.days = days;
	}
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}
	
	

}
