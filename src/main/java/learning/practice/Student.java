package learning.practice;

import java.util.List;
import java.util.stream.Collectors;

public class Student {
	
	private String name;
	private Integer age;
	
	public Student() {
	}

	public static Student of(String name,Integer age) {
		return new Student(name, age);
	}
	
	public static List<Student> of(List<String> students) {
		return students.stream()
				.map(str -> new Student(str.split(";")[0], Integer.parseInt(str.split(";")[1])))
					.collect(Collectors.toList());
	}
	
	public Student(String name, Integer age) {
		super();
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}
	
	
	
	

}
