package learning.domains.movies;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import learning.functions.arrays.FirstLast;

public class Movies {

	 @SuppressWarnings("unused")
	private final Collection<Movie> collection;

	    public static Movies of(Movie... movies) {
	        return new Movies(movies);
	    }

	    private Movies(Movie... movies) {
	        this.collection = Arrays.asList(movies);
	    }
	    
	    public Collection<Movie> selectAll(){
	    	return collection;
	    }
	    
	    public Collection<String> selectTitles() {
	    	List<String> result = new LinkedList<>();
	    	for (Movie movie: collection) {
	    		result.add(movie.title);
	    	}
	   // 	return result;
	    	Stream<Movie> stream = collection.stream();
			Stream<String> titles = stream.map(m -> m.title);
			//Stream<Integer> lengths = titles.map(t -> t.length());
			//lengths.count();
			return titles.collect(Collectors.toList());
	       // throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public FirstLast<Integer> selectDistinctYears() {
	    	throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Collection<String> whereYearIs(int year) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Collection<String> whereTitleContains(String text) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Optional<Movie> selectMaxYear(int year) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Optional<Movie> selectMinYear(int runtime) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Map<Genre,Collection<Movie>> groupByGenre() {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Map<Boolean,Collection<Movie>> havingRuntimeGreaterThan(int minutes) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public Collection<String> whereGenreIs(Genre genre) {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    public BigDecimal sumBudget() {
	        throw new UnsupportedOperationException("It's your job to implement this method"); // TODO
	    }
	    
	    
}
