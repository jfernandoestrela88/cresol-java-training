package learning.domains.movies;

public final class Movie {
	
	public final String title;   
	public final int year; 
	public final int month; 
	public final int day;
	public final Genre[] genres; 
	public final double usaGross; 
	public final boolean estimated;
	public final double budget;  
	public final int runtime;
	
	public static Movie of(String title, int year, int month, int day) {
        return new Movie(title, year, month, day, 0.0, false, 0, 0.0);
    }
	public static Movie of(String title, int year, int month, int day, double budget, boolean estimated, int runtime, double usaGross, Genre ...genres) {
        return new Movie(title, year, month, day, budget, estimated, runtime, usaGross, genres);
    }
	
	  @Override
	public String toString() {
		return "Movie [title=" + title + ", year=" + year + "]";
	}
	private Movie(String title, int year, int month, int day, double budget, boolean estimated, int runtime, double usaGross, Genre ...genres) {
	        this.title = title;
	        this.year = year;
	        this.month = month;
	        this.day = day;
	        this.budget = budget;
	        this.estimated= estimated;
	        this.runtime = runtime;
	        this.usaGross = usaGross;
	        this.genres = genres;
	    }
	  
	public static final Movie.Builder builder(String title){
		return new Movie.Builder(title);
	}
	
	public static class Builder {
		
    	private String title;   
    	private int year; 
    	private int month; 
    	private int day;
    	private Genre[] genres; 
    	private double usaGross;
    	private  boolean estimated;
    	private double budget;  
    	private int runtime;
    	
    	Builder(String title){
    		this.title = title;
    	}
    	public final Builder releasedDate(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
			return this;
		}
    	public final Builder usaGross(double usaGross, boolean estimated) {
    		this.usaGross = usaGross;
    		this.estimated = estimated;
    		return this;
    	}
    	public final Builder budget(double budget) {
    		this.budget = budget;
    		return this;
    	}
    	public final Builder runtime(int runtime) {
    		this.runtime = runtime;
    		return this;
    	}	
    	public final Builder genres(Genre...genres) {
    		this.genres = genres;
    		return this;
    	}	
		public final Movie build() {
			return Movie.of(title, year, month, day, budget, estimated, runtime, usaGross, genres);
		}
	}
	
}